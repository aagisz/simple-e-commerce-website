<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">

<title>UI KIT - Marketplace and Ecommerce html template</title>

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- jQuery -->
<script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

<!-- plugin: fancybox  -->
<script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
<link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<script src="plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- plugin: slickslider -->
<link href="plugins/slickslider/slick.css" rel="stylesheet" type="text/css" />
<link href="plugins/slickslider/slick-theme.css" rel="stylesheet" type="text/css" />
<script src="plugins/slickslider/slick.min.js"></script>

<!-- custom style -->
<link href="css/ui.css" rel="stylesheet" type="text/css"/>
<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="js/script.js" type="text/javascript"></script>

<script type="text/javascript">
/// some script

// jquery ready start
$(document).ready(function() {
	// jQuery code

}); 
// jquery end
</script>

</head>
<body>
    <header class="section-header">
        <section class="header-main">
            <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3">
            <div class="brand-wrap">
                <a href="index.html">
                    <img class="logo" src="images/logo-dark.png" href>
                    <h2 class="logo-text">Ini Logo</h2>
                </a>
            </div> <!-- brand-wrap.// -->
            </div>
            <div class="col-lg-6 col-sm-6">
                <form action="#" class="search-wrap">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                    </div>
                </form> <!-- search-wrap .end// -->
            </div> <!-- col.// -->
            <div class="col-lg-3 col-sm-6">
                <div class="widgets-wrap d-flex justify-content-end">
                    <div class="widget-header">
                        <a href="cart.php" class="icontext">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-shopping-cart"></i></div>
                            <div class="text-wrap">
                                <small>Keranjang</small>
                                <span>3 items</span>
                            </div>
                        </a>
                    </div> <!-- widget .// -->
                    <div class="widget-header dropdown">
                        <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                            <div class="text-wrap">
                                <small>Hello.</small>
                                <span>Login <i class="fa fa-caret-down"></i></span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <form class="px-4 py-3">
                                <div class="form-group">
                                  <label>Email address</label>
                                  <input type="email" class="form-control" placeholder="email@example.com">
                                </div>
                                <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="#">Have account? Sign up</a>
                                <a class="dropdown-item" href="#">Forgot password?</a>
                        </div> <!--  dropdown-menu .// -->
                    </div> <!-- widget  dropdown.// -->
                </div>	<!-- widgets-wrap.// -->	
            </div> <!-- col.// -->
        </div> <!-- row.// -->
            </div> <!-- container.// -->
        </section> <!-- header-main .// -->
        
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark white">
          <div class="container">
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="main_nav">
              <ul class="navbar-nav">
                <a class="nav-link pl-0" href="index.php"> <strong>Semua Kategori</strong></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="makeup.php">Makeup</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="skincare.php">Skincare</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="outfit.php">Outfit</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="hijab.php">Hijab</a>
                </li>
              </ul>
            </div> <!-- collapse .// -->
          </div> <!-- container .// -->
        </nav>
        
        </header> <!-- section-header.// -->
        

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y border-top">
    <div class="container">
    
    <div class="row">
        <main class="col-sm-9">
    
    <div class="card">
    <table class="table table-hover shopping-cart-wrap">
    <thead class="text-muted">
    <tr>
      <th scope="col">Produk</th>
      <th scope="col" width="120">Jumlah</th>
      <th scope="col" width="120">Harga</th>
      <th scope="col" class="text-right" width="200">Aksi</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
    <figure class="media">
        <div class="img-wrap"><img src="images/1.jpg" class="img-thumbnail img-sm"></div>
        <figcaption class="media-body">
            <h6 class="title text-truncate">Product name goes here </h6>
            <dl class="dlist-inline small">
              <dt>Size: </dt>
              <dd>XXL</dd>
            </dl>
            <dl class="dlist-inline small">
              <dt>Color: </dt>
              <dd>Orange color</dd>
            </dl>
        </figcaption>
    </figure> 
        </td>
        <td> 
            <select class="form-control">
                <option>1</option>
                <option>2</option>	
                <option>3</option>	
                <option>4</option>	
            </select> 
        </td>
        <td> 
            <div class="price-wrap"> 
                <var class="price">Rp. 145</var> 
                <small class="text-muted">(Rp.5 each)</small> 
            </div> <!-- price-wrap .// -->
        </td>
        <td class="text-right"> 
        <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> 
        <a href="" class="btn btn-outline-danger"> × Hapus</a>
        </td>
    </tr>
    <tr>
        <td>
    <figure class="media">
        <div class="img-wrap"><img src="images/2.jpg" class="img-thumbnail img-sm"></div>
        <figcaption class="media-body">
            <h6 class="title text-truncate">Product name goes here </h6>
            <dl class="dlist-inline small">
              <dt>Size: </dt>
              <dd>XXL</dd>
            </dl>
            <dl class="dlist-inline small">
              <dt>Color: </dt>
              <dd>Orange color</dd>
            </dl>
        </figcaption>
    </figure> 
        </td>
        <td> 
            <select class="form-control">
                <option>1</option>
                <option>2</option>	
                <option>3</option>	
                <option>4</option>	
            </select> 
        </td>
        <td> 
            <div class="price-wrap"> 
                <var class="price">Rp. 35</var> 
                <small class="text-muted">(Rp.10 each)</small> 
            </div> <!-- price-wrap .// -->
        </td>
        <td class="text-right"> 
        <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> 
        <a href="" class="btn btn-outline-danger btn-round"> × Hapus</a>
        </td>
    </tr>
    <tr>
        <td>
    <figure class="media">
        <div class="img-wrap"><img src="images/3.jpg" class="img-thumbnail img-sm"></div>
        <figcaption class="media-body">
            <h6 class="title text-truncate">Product name goes here </h6>
            <dl class="dlist-inline small">
              <dt>Size: </dt>
              <dd>XXL</dd>
            </dl>
            <dl class="dlist-inline small">
              <dt>Color: </dt>
              <dd>Orange color</dd>
            </dl>
        </figcaption>
    </figure> 
        </td>
        <td> 
            <select class="form-control">
                <option>1</option>
                <option>2</option>	
                <option>3</option>	
                <option>4</option>	
            </select> 
        </td>
        <td> 
            <div class="price-wrap"> 
                <var class="price">Rp. 45</var> 
                <small class="text-muted">(Rp.15 each)</small> 
            </div> <!-- price-wrap .// -->
        </td>
        <td class="text-right"> 
            <a data-original-title="Save to Wishlist" title="" href="" class="btn btn-outline-success" data-toggle="tooltip"> <i class="fa fa-heart"></i></a> 
            <a href="" class="btn btn-outline-danger btn-round"> × Hapus</a>
        </td>
    </tr>
    </tbody>
    </table>
    </div> <!-- card.// -->
    
        </main> <!-- col.// -->
        <aside class="col-sm-3">
    <p class="alert alert-success">Tambah 3 item dengan minimal harga Rp.50.000. Untuk mendapatkan Ongkir Gratis keseluruh indonesia</p>
    <dl class="dlist-align">
      <dt>Total Harga: </dt>
      <dd class="text-right">Rp. 568</dd>
    </dl>
    <dl class="dlist-align">
      <dt>Diskon:</dt>
      <dd class="text-right">Rp. 658</dd>
    </dl>
    <dl class="dlist-align h4">
      <dt>Total:</dt>
      <dd class="text-right"><strong>Rp. 1,650</strong></dd>
    </dl>
    <hr>
    
        </aside> <!-- col.// -->
    </div>
    
    </div> <!-- container .//  -->
    </section>
    <!-- ========================= SECTION CONTENT END// ========================= -->

<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-dark">
	<div class="container">
		<section class="footer-top padding-top">
			<div class="row">
				<aside class="col-sm-3 col-md-3 white">
                <h5 class="title">Kategori</h5>
					<ul class="list-unstyled">
						<li> <a href="makeup.php">Makeup</a></li>
						<li> <a href="skincare.php">Skincare</a></li>
						<li> <a href="outfit.php">Outfit</a></li>
						<li> <a href="hijab.php">Hijab</a></li>
					</ul>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
				</aside>
				<aside class="col-sm-3  col-md-3 white">
				</aside>
				<aside class="col-sm-3">
					<article class="white">
						<h5 class="title">Contacts</h5>
						<p>
							<strong>Phone: </strong> +123456789 <br> 
						    <strong>Fax:</strong> +123456789
						</p>

						 <div class="btn-group white">
						    <a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>
						    <a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>
						    <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
						    <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>
						</div>
					</article>
				</aside>
			</div> <!-- row.// -->
			<br> 
		</section>
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->





</body>
</html>